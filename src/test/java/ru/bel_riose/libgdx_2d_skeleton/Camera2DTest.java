/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.libgdx_2d_skeleton;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxNativesLoader;
import static junit.framework.Assert.assertTrue;
import junit.framework.TestCase;
import ru.bel_riose.geometry.Vector;

/**
 *
 * @author Ivan
 */
public class Camera2DTest extends TestCase {

    public Camera2DTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        GdxNativesLoader.load();

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCameraMatrix() {
        Camera2D camera = new Camera2D(20, -500, 0.5,0, 1280, 1024);
        Vector v = new Vector(100, 100);
        Vector vMtS = camera.mathToScr(v);
        Matrix4 matr = camera.getViewMatrix();
        Vector3 v3 = new Vector3((float) v.x, (float) v.y, 0);
        Vector3 vMatr = v3.prj(matr);
        System.out.println("");
        System.out.println("Camera matrix test:");
        System.out.println("Matrix result: " + String.valueOf(vMatr.x) + ", " + String.valueOf(vMatr.y) + ", " + String.valueOf(vMatr.z));
        System.out.println("mathToScreen result: " + String.valueOf(vMtS.x) + ", " + String.valueOf(vMtS.y));
        System.out.println("");
        assertTrue(vMatr.x == vMtS.x);
        assertTrue(vMatr.y == vMtS.y);
        assertTrue(vMatr.z == 0);
    }

    public void testCameraRevert() {
        Camera2D camera = new Camera2D(20, -500, 0.5,0, 1280, 1024);
        Vector v = new Vector(100, 100);
        assertTrue(v.equals(camera.scrToMath(camera.mathToScr(v))));
        assertTrue(v.equals(camera.mathToScr(camera.scrToMath(v))));
    }
}

