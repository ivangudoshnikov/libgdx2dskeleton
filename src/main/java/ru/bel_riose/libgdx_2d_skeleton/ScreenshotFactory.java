/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.libgdx_2d_skeleton;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import java.nio.ByteBuffer;

/**
 *
 */
public class ScreenshotFactory {

    private static int counter = 1;

    public static void saveScreenshot(int x, int y, int w, int h) {
        saveScreenshot("screenshot" + counter++ + ".png", x, y, w, h);
    }

    public static void saveScreenshot(String filename, int x, int y, int w, int h) {
        try {
            FileHandle fh= new FileHandle(filename);
            if (fh.exists())fh.delete();
            Pixmap pixmap = getScreenshot(x, y, w, h,true);
            PixmapIO.writePNG(fh, pixmap);
            pixmap.dispose();
        } catch (Exception e) {
        }
    }

    private static Pixmap getScreenshot(int x, int y, int w, int h,boolean flipY) {
         Gdx.gl.glPixelStorei(GL10.GL_PACK_ALIGNMENT, 1);
                
                final Pixmap pixmap = new Pixmap(w, h, Format.RGBA8888);
                ByteBuffer pixels = pixmap.getPixels();
                Gdx.gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, pixels);
                
                final int numBytes = w * h * 4;
                byte[] lines = new byte[numBytes];
                if (flipY) {
                        final int numBytesPerLine = w * 4;
                        for (int i = 0; i < h; i++) {
                                pixels.position((h - i - 1) * numBytesPerLine);
                                pixels.get(lines, i * numBytesPerLine, numBytesPerLine);
                        }
                        pixels.clear();
                        pixels.put(lines);
                } else {
                        pixels.clear();
                        pixels.get(lines);
                }
                
                return pixmap;
    }
}
