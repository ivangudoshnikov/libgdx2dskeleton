/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.libgdx_2d_skeleton;

import com.badlogic.gdx.math.Matrix4;
import ru.bel_riose.geometry.Vector;

/**
 *
 * @author Bel_Riose
 */
public class Camera2D {

    private Matrix4 projectionMatrix, viewMatrix;
    protected double k, width, height,angle;
    protected Vector d;

    public Vector mathToScr(double x, double y) {
        return new Vector((x - d.x) / k, (y - d.y) / k).rotateMe(angle);
    }

    public Vector mathToScr(Vector p) {
        return mathToScr(p.x, p.y);
    }

    public Vector mouseToScr(double x, double y) {
        return new Vector(x - width / 2, height / 2 - y);
    }

    public Vector scrToMath(double x, double y) {
        Vector rotated=new Vector(x, y).rotateMe(-angle);
        return new Vector(rotated.x * k + d.x, rotated.y * k + d.y);
    }

    public Vector scrToMath(Vector p) {
        return scrToMath(p.x, p.y);
    }

    public double rescaleMathToScr(double x) {
        return x / k;
    }

    public double rescaleScrToMath(double x) {
        return x*k;
    }
    
    public void setD(Vector v) {
        d=new Vector(v);
        updateViewMatrix();
    }
    
    public Vector getD(){
        return new Vector(d);
    }
        
    public double getK() {
        return k;
    }

    public void setK(double k) {
        this.k = k;
        updateViewMatrix();
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
        updateViewMatrix();
    }

    /**
     *
     * @param dx сдвиг камеры по горизонтали
     * @param dy сдвиг камеры по вертикали
     * @param k зум
     * @param angle угол поворота
     * @param width ширина вьюпорта
     * @param height высота вьюпорта
     */
    public Camera2D(double dx, double dy, double k, double angle, double width, double height) {
        d=new Vector(dx, dy);
        this.k = k;
        this.angle=angle;
        this.width = width;
        this.height = height;
        projectionMatrix = new Matrix4();
        viewMatrix = new Matrix4();
        updateProjectionMatrix();
        updateViewMatrix();
    }

    public void resize(float w, float h) {
        this.width = w;
        this.height = h;
        updateProjectionMatrix();
    }

    protected final void updateProjectionMatrix() {
        projectionMatrix.setToOrtho2D(-(float) (width / 2), -(float) (height) / 2, (float) width, (float) height);
    }

    protected final void updateViewMatrix() {
        viewMatrix.setToScaling((float) (1 / k), (float) (1 / k), 1);
        viewMatrix.rotate(0, 0, 1,(float)Math.toDegrees(angle));
        viewMatrix.translate(-(float) d.x, -(float) d.y, 0);
    }

    public Matrix4 getViewProjectionMatrix() {
        return projectionMatrix.cpy().mul(viewMatrix);
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    Matrix4 getProjectionMatrix() {
        return projectionMatrix;
    }

    Matrix4 getViewMatrix() {
        return viewMatrix;
    }
}
