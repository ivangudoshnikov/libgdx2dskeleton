/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.libgdx_2d_skeleton;

import com.badlogic.gdx.InputProcessor;

/**
 *
 * @author Bel_Riose
 */
public abstract class InputHandler<S extends Screen> implements InputProcessor{
    protected final S screen;

    public InputHandler(S screen) {
        this.screen = screen;
    }
    
}
