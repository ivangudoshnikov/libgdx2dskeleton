/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.libgdx_2d_skeleton;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

/**
 *
 * @author Bel_Riose
 * @param <S>
 */
public abstract class MapInputHandler<S extends Screen> extends InputHandler<S> {

    public MapInputHandler(S screen) {
        super(screen);
    }
    
    public class DragHandler {

        public int lastX, lastY, button;

        public void down(int x, int y, int button) {
            this.lastX = x;
            this.lastY = y;
            this.button = button;
        }

        public void drag(int x, int y) {
            this.lastX = x;
            this.lastY = y;
        }
    }
    protected DragHandler dragHandler = new DragHandler();    
    
    protected abstract MapCamera2D getCamera();

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        dragHandler.down(screenX, screenY, button);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (dragHandler.button == Input.Buttons.MIDDLE) {
            getCamera().drag(screenX, screenY, dragHandler.lastX, dragHandler.lastY);
        }
        dragHandler.drag(screenX, screenY);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        getCamera().scroll(amount, Gdx.input.getX(), Gdx.input.getY());
        return true;
    }
}
