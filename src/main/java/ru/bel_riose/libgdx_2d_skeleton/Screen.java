/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.libgdx_2d_skeleton;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 *
 * @author Bel_Riose
 * @param <G>
 * @param <C>
 * @param <I>
 * @param <GUI>
 */
public abstract class Screen<G extends Game, C extends Camera2D, I extends InputHandler, GUI extends GraphicalUserInterface> implements com.badlogic.gdx.Screen {

    protected final G application;
    protected C camera2d;
    protected I defaultInputHandler;
    protected GUI gui;
    protected final ShapeRenderer shapeRenderer;
    protected final SpriteBatch spriteBatch;

    public Screen(G application) {
        this.application = application;
        shapeRenderer = new ShapeRenderer();
        spriteBatch = new SpriteBatch();
    }

    public G getApplication() {
        return application;
    }

    public C getCamera2d() {
        return camera2d;
    }

    public GUI getGui() {
        return gui;
    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
        spriteBatch.dispose();
        gui.dispose();
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void show() {
        setDeafultInputProcessor();
    }

    @Override
    public void resize(int width, int height) {
        gui.getStage().setViewport(width, height, true);
        camera2d.resize(width, height);
    }

    public void setDeafultInputProcessor() {
        Gdx.input.setInputProcessor(new InputMultiplexer(gui.stage, defaultInputHandler));
    }
}
