/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.libgdx_2d_skeleton;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * @author Ivan gui=stage+skin где рисовать и как рисовать
 */
public abstract class GraphicalUserInterface<S extends Screen> {
    protected final S screen;
    protected final Stage stage;
    private static Skin skin;

    public GraphicalUserInterface(S screen) {
        this.screen=screen;
        stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
    }

    public abstract void update();

    public void resize(float w, float h) {
        stage.setViewport(w, h);
    }

    public void actNdraw() {
        stage.act();
        stage.draw();
    }

    public Skin getSkin() {
        if (skin == null) {
            skin = new Skin(Gdx.files.classpath("skins/uiskin"));
        }
        return skin;
    }
    
    public Stage getStage(){
        return stage;
    }

    public S getScreen() {
        return screen;
    }
    
    public void dispose(){
        stage.dispose();
        skin.dispose();
    }
}
