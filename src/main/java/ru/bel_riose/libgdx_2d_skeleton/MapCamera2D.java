/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.libgdx_2d_skeleton;

import ru.bel_riose.geometry.Vector;

/**
 *
 * @author Bel_Riose
 */
public class MapCamera2D extends Camera2D {

    protected double defaultK, defaultAngle;
    protected Vector defaultD;

    public MapCamera2D(double dx, double dy, double k,double angle, double width, double height) {
        super(dx, dy, k,angle, width, height);
        defaultD=new Vector(dx,dy);
        defaultK = k;
        defaultAngle=angle;
    }

    public void drag(int screenX, int screenY, int lastX, int lastY) {
        Vector drag=Vector.diff(scrToMath(mouseToScr(screenX,screenY)), scrToMath(mouseToScr(lastX, lastY)));
        d.substractFromMe(drag);
        updateViewMatrix();
    }

    public void scroll(int amount, int mouseX, int mouseY) {
        double k1 = (k * Math.exp(-amount * 0.3));
        Vector drag = mouseToScr(mouseX, mouseY).rotateMe(-angle);
        d.substractFromMe(drag.multMe(k1-k));
        //d.x = d.x - (k1 - k) * mouse.x;
        //d.y = d.y - (k1 - k) * mouse.y;
        k = k1;
        updateViewMatrix();
    }

    public void restoreDefaults() {
        d=new Vector(defaultD);
        k = defaultK;
        angle=defaultAngle;
        updateViewMatrix();
    }
}
