/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.bel_riose.libgdx_2d_skeleton.sample_app;

import com.badlogic.gdx.Game;
import ru.bel_riose.geometry.Vector;

/**
 *
 * @author Ivan
 */
public class MyGame extends Game{
    private GameScreen screen;
    Vector clicked;
    @Override
    public void create() {
        screen=new GameScreen(this);
        setScreen(screen);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render() {
        super.render(); 
    }

    @Override
    public void resume() {
        super.resume(); 
    }

    @Override
    public void pause() {
        super.pause(); 
    }

    @Override
    public void dispose() {
        super.dispose(); 
    }
    
}
