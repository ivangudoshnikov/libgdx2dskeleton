package ru.bel_riose.libgdx_2d_skeleton.sample_app;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * Hello world!
 *
 */
class App 
{
    public static void main( String[] args )
    {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.useGL20 = true;
                config.fullscreen=false;
                config.width=600;
                config.height=900;
//                config.fullscreen=true;
//                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//                config.width=screenSize.width;
//                config.height=screenSize.height;
                new LwjglApplication(new MyGame(), config);
    }
}
