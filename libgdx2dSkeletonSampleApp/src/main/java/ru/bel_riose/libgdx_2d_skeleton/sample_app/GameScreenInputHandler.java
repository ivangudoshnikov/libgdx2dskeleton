/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.libgdx_2d_skeleton.sample_app;

import com.badlogic.gdx.Input;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.MapInputHandler;

/**
 *
 * @author Ivan
 */
public class GameScreenInputHandler extends MapInputHandler<GameScreen> {

    public GameScreenInputHandler(GameScreen screen) {
        super(screen);
    }

    @Override
    protected MapCamera2D getCamera() {
        return screen.getCamera2d();
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.BACKSPACE:
                getCamera().restoreDefaults();
                break;
            case Input.Keys.ESCAPE:
                System.exit(0);
                break;
            case Input.Keys.Q:
                getCamera().setAngle(getCamera().getAngle() - Math.PI / 30);
                break;
            case Input.Keys.E:
                getCamera().setAngle(getCamera().getAngle() + Math.PI / 30);
                break;
        }
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        super.touchDown(screenX, screenY, pointer, button);
        if (button == Input.Buttons.LEFT) {
            screen.getApplication().clicked = getCamera().scrToMath(getCamera().mouseToScr(screenX, screenY));
        }
        return true;
    }
}
