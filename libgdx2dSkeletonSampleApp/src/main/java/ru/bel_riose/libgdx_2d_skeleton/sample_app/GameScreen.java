/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.libgdx_2d_skeleton.sample_app;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import ru.bel_riose.libgdx_2d_skeleton.MapCamera2D;
import ru.bel_riose.libgdx_2d_skeleton.Screen;

/**
 *
 * @author Ivan
 */
public class GameScreen extends Screen<MyGame, MapCamera2D, GameScreenInputHandler, GameScreenGUI> {

    public GameScreen(MyGame application) {
        super(application);
        camera2d = new MapCamera2D(0, 0, 1,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        defaultInputHandler = new GameScreenInputHandler(this);
        gui = new GameScreenGUI(this);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        shapeRenderer.setProjectionMatrix(camera2d.getViewProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(1, 1, 1, 1);
        shapeRenderer.line(-10, -10, 10, 10);
        shapeRenderer.line(-20, 20, 20, -20);
        shapeRenderer.line(-(float)camera2d.getWidth()/2, -(float)camera2d.getHeight()/2,(float)camera2d.getWidth()/2,-(float)camera2d.getHeight()/2);
        shapeRenderer.line(-(float)camera2d.getWidth()/2, -(float)camera2d.getHeight()/2,-(float)camera2d.getWidth()/2,(float)camera2d.getHeight()/2);
        
        shapeRenderer.end();
        if (application.clicked != null) {
            shapeRenderer.setColor(1, 0, 0, 1);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
            shapeRenderer.point((float) application.clicked.x, (float) application.clicked.y, 0);
            
            shapeRenderer.end();
        }
    }

}
